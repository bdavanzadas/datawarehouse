import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Scanner;
import negocio.ETL_Process;
import persistencia.ConexionSQLite;

/**
 * 
 */

/**
 * @author Sergio Fernández
 *
 */
public class Main {

	//Variables para crear los reportes
	private static FileWriter reporte;
	private static PrintWriter rep;
	
	// Ficheros sobre los que se opera durante la ETL
	final static String [] LIST_SOURCE = {"test/menors_2016.csv"};
	final static String PATH_DATA_AUX = "test/exchange_data.sqlite";
	final static String PATH_DATA_WAREHOUSE = "test/ContratacionBCN.sqlite";
	
	
	// Consultas sobre el Data Warehouse
	final static String TITULO_CONSULTA_01 = "Mes en el que se hace mayor numero de contratos en el año.";
	final static String CONSULTA_01 = "SELECT Fecha.año AS 'Año', Fecha.mes AS 'Mes', MAX(Contrato.totalContratos) AS 'Total de contratos' "
			+ "FROM Contrato "
			+ "LEFT JOIN Fecha ON Contrato.id_fecha=Fecha.id_fecha  GROUP BY  Fecha.año";
	
	final static String TITULO_CONSULTA_02 = "Coste medio al mes en contratos por cada contratante.";
	final static String CONSULTA_02 = "SELECT OrganoContratante.organoContratante AS 'Organo Contratante', Fecha.mes AS 'Mes', AVG(ObjetoContrato.importe) AS 'Media del coste' "
			+ "FROM Contrato "
			+ "LEFT JOIN Fecha ON Contrato.id_fecha=Fecha.id_fecha "
			+ "LEFT JOIN OrganoContratante ON Contrato.id_organoContratante=OrganoContratante.id_organoContratante "
			+ "LEFT JOIN ObjetoContrato ON Contrato.id_objetoContrato=ObjetoContrato.id_objetoContrato "
			+ "GROUP BY OrganoContratante.organoContratante, Fecha.mes;";
	
	final static String TITULO_CONSULTA_03 = "Distritos con mayor gasto.";
	final static String CONSULTA_03 = "SELECT OrganoContratante.organoContratante AS 'Organo Contratante', Sum(ObjetoContrato.importe) AS 'Total de gasto' "
			+ "FROM Contrato LEFT JOIN OrganoContratante ON Contrato.id_organoContratante=OrganoContratante.id_organoContratante "
			+ "LEFT JOIN ObjetoContrato ON Contrato.id_objetoContrato=ObjetoContrato.id_objetoContrato "
			+ "where SUBSTR(organoContratante, 1, 9)='Districte' "
			+ "GROUP BY OrganoContratante.organoContratante ORDER BY Sum(ObjetoContrato.importe) ASC";

	final static String TITULO_CONSULTA_04 ="Gasto de cada uno de los organismos en cada trimestre.";
	final static String CONSULTA_04 = "SELECT OrganoContratante.organoContratante AS 'Organo Contratante', Fecha.trimestre AS 'Trimestre', Sum(ObjetoContrato.importe) AS 'Total de gasto' "
			+ "FROM Contrato "
			+ "LEFT JOIN Fecha ON Contrato.id_fecha=Fecha.id_fecha "
			+ "LEFT JOIN OrganoContratante ON Contrato.id_organoContratante=OrganoContratante.id_organoContratante "
			+ "LEFT JOIN ObjetoContrato ON Contrato.id_objetoContrato=ObjetoContrato.id_objetoContrato "
			+ "GROUP BY OrganoContratante.organoContratante, Fecha.trimestre;";
	
	// Conexiones a la BD del Data Warehouse
	private static ConexionSQLite cn_dw;
	private static Connection conexion_dw;

	
	private static Scanner leer=new Scanner(System.in);
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public static void main(String[] args) throws IOException {
		
		int operacion = -1;
		//Creamos la calse para la ETL (Extraction, Transformation and Loading)
		try {
			ETL_Process Etl_process= new ETL_Process(LIST_SOURCE,PATH_DATA_AUX, PATH_DATA_WAREHOUSE);
			
			while (operacion!=0){
				menu_operaciones();
				operacion = leer.nextInt();
						 
				switch (operacion){
					case 0:
						break;
						
					case 1: //Comenzar con la EXTRACCIÓN de datos a partir de las fuentes.
						//Etl_process.executeExtraction();
						break;
						
					case 2: //Comenzar con la TRANSFORMACIÓN a partir de los datos extraidos.
						//Etl_process.executeTransformationAndLoad();
						break;
						
					case 3: //Realizar el proceso de CARGA de los datos después de la transformación,
						//Etl_process.executeTransformationAndLoad();
						break;
						
					case 4: //Realizar el proceso ETL por completo
						//Etl_process.executeETL();
						break;
					
					case 5: // Realizar las consultas.
						consultas();	
						break;
						
					default:
						System.out.println("\t[!] Opción incorrecta, seleccione entre [0- 4] [!]");
						operacion= -1;
						leer.nextLine();
						break;
				}
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static void consultas() throws ClassNotFoundException, SQLException, IOException {
		int consulta = -1;
		cn_dw = new ConexionSQLite(PATH_DATA_WAREHOUSE);
		Runtime runTime;
		Process p;
		
		ResultSet result = null;
		
		try {
			while (consulta!=0){
				conexion_dw = cn_dw.getConexion();
				
				leer.nextLine();
				menu_consultas();
				consulta = leer.nextInt();
				PreparedStatement stt = null;
				switch (consulta){
					case 0:
						stt = conexion_dw.prepareStatement("");
						break;
						
					case 1: //
						reporte = new FileWriter("consulta1.html");
						stt = conexion_dw.prepareStatement(CONSULTA_01);
						result = stt.executeQuery();
						mostrarConsulta(result, TITULO_CONSULTA_01);
						
						runTime = Runtime.getRuntime(); 
						p = runTime.exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe "+ "consulta1.html");
						break;
						
					case 2: //
						reporte = new FileWriter("consulta2.html");
						stt = conexion_dw.prepareStatement(CONSULTA_02);
						result = stt.executeQuery();
						mostrarConsulta(result, TITULO_CONSULTA_02);
						
						runTime = Runtime.getRuntime(); 
						p = runTime.exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe "+ "consulta2.html");

						break;
						
					case 3: // 
						reporte = new FileWriter("consulta3.html");
						stt = conexion_dw.prepareStatement(CONSULTA_03);
						result = stt.executeQuery();
						mostrarConsulta(result, TITULO_CONSULTA_03);
						
						runTime = Runtime.getRuntime(); 
						p = runTime.exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe "+ "consulta3.html");
						
						break;
						
					case 4: //
						reporte = new FileWriter("consulta4.html");
						stt = conexion_dw.prepareStatement(CONSULTA_04);
						result = stt.executeQuery();
						mostrarConsulta(result, TITULO_CONSULTA_04);
						
						
						runTime = Runtime.getRuntime(); 
						p = runTime.exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe "+ "consulta4.html");

						break;				
						
					default:
						System.out.println("\t[!] Opción incorrecta, seleccione entre [0- 4] [!]");
						consulta= -1;
						
				}
				stt.close();	
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	private static void mostrarConsulta(ResultSet result, String tituloConsulta) throws SQLException {
		int i = 0;
		int n_columnas = result.getMetaData().getColumnCount();
		String [] cabecera = new String[n_columnas];
		
		for (i=1; i<=n_columnas; i++){
			cabecera[i-1] = result.getMetaData().getColumnName(i);
		}

		// Escribimos la cabecera del documento HTML.
		escribirCabecera(cabecera, tituloConsulta);
	
		// Recorremos los resultados de la consulta
		while (result.next()) {
			LinkedList<String> res_columnas = new LinkedList<String>();
			for(int j=0; j<n_columnas; j++){
				res_columnas.add(j, result.getString(j+1));
			}
			escribirFila(res_columnas);
		}
		// Cerramos el documentoHTML;
		cerrarDocumento();
	}

	private static void menu_consultas() {
		System.out.println("\n\t\t===[ MENÚ CONSULTAS ]===");
		System.out.println("1.- "+ TITULO_CONSULTA_01);
		System.out.println("2.- "+ TITULO_CONSULTA_02);
		System.out.println("3.- "+ TITULO_CONSULTA_03);
		System.out.println("4.- "+ TITULO_CONSULTA_04);
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opción [0-4]: ");
	}
	
	private static void menu_operaciones() {
		System.out.println("\n\t\t===[ MENÚ PRINCIPAL ]===");
		System.out.println("1.- Extracción de los datos a partir de las fuentes.");
		System.out.println("2.- Transformación de los datos a partir de las fuentes");
		System.out.println("3.- Carga de los datos a partir de la transformación de los datos extraidos a partir de las fuentes");
		System.out.println("4.- Realizar proceso ETL completo.");
		System.out.println("5.- Realizar consultas una vez terminado el proceso completo ETL [{1,2,3} o {4}]");
		System.out.println("");
		System.out.println("0.- Salir");
		System.out.println("");
		System.out.print("  [*] Elija una opción [0-5]: ");
	}

	private static void escribirCabecera(String[] cabecera, String tituloConsulta) {
		rep = new PrintWriter(reporte);

		rep.println("<html>"+
				"<body>"+
				"<center>"+
				"<H1>"+
				"Reporte: " + tituloConsulta +
				"</H1>"+
				"<br>"+
				"<table border= \"2\" cellpadding=\"5\">"+
				"	<tr>");
		for(int i=0; i< cabecera.length; i++){
			rep.println("	<td><center><b>"+cabecera[i]+"</b></center></td>");
		}
		rep.println("</tr>");
	}
	
	private static void escribirFila(LinkedList<String> res_columnas) {
		rep = new PrintWriter(reporte);

		rep.println("	<tr>");
		for(int i=0; i<res_columnas.size(); i++){
			rep.println("	<td>"+res_columnas.get(i)+"</td>");
		}
		rep.println("</tr>");
	}
		
	private static void cerrarDocumento(){

		try
		{
			rep = new PrintWriter(reporte);

			rep.println("</center>"+
						"</body>" +
						"</html>");
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (null != reporte)
					reporte.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}

		}
	}
}
