/**
 * 
 */
package negocio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergio Fernández.
 *
 */
public class Extraction_Process {

	private static List<Contrato> contratos = new ArrayList<Contrato>();
	final static int STRING = 0;
	final static int INTEGER = 1;
	final static int DOUBLE = 2;
	final static int FLOAT = 3;
	
	private String [] sources;

	
	public Extraction_Process(String [] sources){//, String path_data_aux){
		this.sources= sources;
	}

	/**
	 * @return the contratos
	 */
	public static List<Contrato> getContratos() {
		return contratos;
	}

	/**
	 * @param contratos the contratos to set
	 */
	public static void setContratos(List<Contrato> contratos) {
		Extraction_Process.contratos = contratos;
	}

	/**
	 * @return the sources
	 */
	public String[] getSources() {
		return sources;
	}

	/**
	 * @param sources the sources to set
	 */
	public void setSources(String[] sources) {
		this.sources = sources;
	}

	protected List<Contrato> execute() {
		int i = 0; 
		for (i=0; i < sources.length; i++){
			leerCSV(sources[i]);
		}
		return contratos;
	}
	
	private void leerCSV(String src) {  
        try {
	        String separator=";";
	        char c = separator.charAt(0);
	       
	        CsvReader source_import=new CsvReader(src, c, StandardCharsets.UTF_8);
	        source_import.readHeaders();
	         
	        while (source_import.readRecord())
	        {
	        	String tipoEns=verificar(source_import.get(0),STRING);
	        	String organo_contratante=verificar(source_import.get(1),STRING); 
	        	String tipo_contrato=verificar(source_import.get(2),STRING);
	        	String fecha_adjudicacion=comprobarfecha(verificar(source_import.get(3),STRING)); 
	        	String proveedor=verificar(source_import.get(4),STRING);
	        	String nif=verificar(source_import.get(5),STRING); 
	        	String objeto_de_contrato=verificar(source_import.get(6),STRING); 
	        	float importe_adjudicado=Float.parseFloat(verificar(source_import.get(7),FLOAT));
	        	 
	        	
	            contratos.add(new Contrato(tipoEns, organo_contratante, tipo_contrato, fecha_adjudicacion, proveedor, nif, objeto_de_contrato, importe_adjudicado));       
	        }
	        
	        source_import.close();	         
        
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	private static String comprobarfecha(String date) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String date_aux = "";

		if(date != null){	
			if(date.length()==10){
				//Comprobamos y corregimos errores del tipo 12703/2010 u otros formatos como 12-03-2010
				date_aux = date_aux.concat(date.substring(0, 2)+"/");
				date_aux = date_aux.concat(date.substring(3, 5)+"/");
				date = date_aux.concat(date.substring(6,10));

				if (date.matches("[0-3]\\d/[01]\\d/\\d{4}")){
					df.setLenient(false);
					try {
						df.parse(date);
					} catch (ParseException ex) {
						date = "";
					}
				}
			}
			else{
				System.out.println("date:" + date);
				date="";
			}
		}

		return date;
	}

	private static String verificar(String data, int type) {
		String data_string=null;
		int data_int =0;
		int decimales =0;
		double data_double=0.00;
		float data_float = 0.00f;

		switch (type) {
		case STRING:	

			if(data==null) data="";
			data_string = data.contains("\"") ? data.replaceAll("\"", "") : data;

			break;
		case INTEGER:
			try{
				if(data==null) data="0";
				data_int = Integer.parseInt(data);
			}catch (Exception e) {

				data_double=0;
			}finally {
				data_string = String.valueOf(data_int);
			}
			break;
		case DOUBLE:
			try{
				if(data==null) data="0";
				data_double = Double.parseDouble(data);
			}catch (Exception e) {
				data_double=0;
			}finally {
				data_string = String.valueOf(data_double);
			}
			break;
		case FLOAT:
			try{				
				if(data==null) data="0";
				if(data.equals("")) data = "0";
				String data_aux="";
				data= data.replaceAll(" ", "");
				data = data.contains("€") ? data.replaceAll("€","") : data;
				data = data.contains(",") ? data.replaceAll(",",".") : data;

				decimales = (data.lastIndexOf(".") < 0) ? 0 : data.length()-data.lastIndexOf(".");

				if(data.substring(0, data.length()-decimales).contains(".")){
					data_aux = data.substring(0, data.length()-decimales);
					decimales= data_aux.lastIndexOf(".");
					data_aux = data_aux.replace(".", "");
					decimales= data.length()-data.lastIndexOf(".");

					data = data_aux.concat(data.substring(data.length()-decimales));
				}

				data_float = Float.parseFloat(data);
			}catch (Exception e) {
				data_float=0f;
			}finally {
				data_string = String.valueOf(data_float);
			}
			break;
		}
		return data_string;
	}
}
