/**
 * 
 */
package negocio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import persistencia.ConexionSQLite;

/**
 * @author Sergio Fernández
 *
 */
public class Load_Process {

	private ConexionSQLite cn_aux;
	private Connection conexion_aux;
	private ConexionSQLite cn_dw;
	private Connection conexion_dw;

	
	public Load_Process(String path_data_aux, String path_data_warehouse) throws ClassNotFoundException, SQLException{
		cn_aux = new ConexionSQLite(path_data_aux);
		conexion_aux = cn_aux.getConexion();
		
		cn_dw = new ConexionSQLite(path_data_warehouse);
		conexion_dw = cn_dw.getConexion();
	}
	
	protected void execute(){
		System.out.println("Crea Dimensiones");
		crearDimensionesBD();
		System.out.println("Crea Hechos");
		crearHechosBD();
		System.out.println("Calcula Medidas");
		CalcularMedidas();
		System.out.println("Termina Execute");
	}
	
	private void crearDimensionesBD(){
		System.out.println("01/06");
		crearTablaFecha();
		System.out.println("02/06");
   		crearTablaProveedor();
   		System.out.println("03/06");
   		crearTablaTipoEns();
   		System.out.println("04/06");
   		crearTablaObjetoContrato();
   		System.out.println("05/06");
   		crearTablaTipoContrato();
   		System.out.println("06/06");
   		crearTablaOrganoContratante();
	}

	public void crearTablaFecha(){

		ResultSet result = null;
		String id="";
		int dia=0;
		int mes=0;
		int anio= 0;

		int trimestre=0;
		String dia_str = "";
		String mes_str = "";
		int dia_semana = 0;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select data_adjudicacio from CONTRATOS GROUP BY data_adjudicacio");
			result = stt.executeQuery();
			while (result.next()) {
				String fecha=result.getString("data_adjudicacio"); 
				if(!fecha.equals("")){
					Date date=ParseFecha(fecha);

					String [] f=fecha.split("/");
					dia=Integer.parseInt(f[0]);
					mes=Integer.parseInt(f[1]);
					anio=Integer.parseInt(f[2]);
	
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(date);
					dia_semana= cal.get(Calendar.DAY_OF_WEEK);
	
					
					if(mes==1 || mes==2 || mes==3) trimestre = 1;
					else if(mes==4 || mes==5 || mes==6) trimestre = 2;
					else if(mes==7 || mes==8 || mes==9) trimestre = 3;
					else if(mes==10 || mes==11 || mes==12) trimestre = 4;
	
					dia_str = (dia<10) ? "0" + Integer.toString(dia) : Integer.toString(dia);
					mes_str = (mes<10) ? "0" + Integer.toString(mes) : Integer.toString(mes);
					
					id=anio+""+mes_str+""+dia_str+"";
					
				}
				else{
					id = "00000000";
					trimestre = 0;
				}

				
				PreparedStatement st = conexion_dw.prepareStatement("insert into Fecha (id_fecha, dia_semana, dia, mes, año, trimestre) values (?,?,?,?,?,?)");

				st.setInt(1, Integer.parseInt(id));
				st.setInt(2, dia_semana);
				st.setInt(3, dia);
				st.setInt(4, mes);
				st.setInt(5, anio);
				st.setInt(6, trimestre);
				st.execute();
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}	 

	}

	private void crearTablaProveedor(){
		ResultSet result = null;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select proveidor, NIF from CONTRATOS GROUP BY NIF");
			result = stt.executeQuery();
			while (result.next()) {
				String proveedor=result.getString("proveidor");
				String NIF=result.getString("nif");

				PreparedStatement st = conexion_dw.prepareStatement("insert into Proveedor (NIF, nombre_prov) values (?,?)");
				st.setString(1, NIF);
				st.setString(2, proveedor);
				st.execute();


			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private void crearTablaTipoEns(){
		ResultSet result = null;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select tipus_ens from CONTRATOS GROUP BY tipus_ens");
			result = stt.executeQuery();
			while (result.next()) {
				String tipo_ens=result.getString("tipus_ens");

				PreparedStatement st = conexion_dw.prepareStatement("insert into TipoEns (tipoEns) values (?)");
				st.setString(1, tipo_ens);
				st.execute();
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	
	private void crearTablaObjetoContrato(){
		ResultSet result = null;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select objecte_del_contracte, import_adjudicat from CONTRATOS");
			result = stt.executeQuery();
			while (result.next()) {
				String objeto_contrato=result.getString("objecte_del_contracte");
				Float importe=result.getFloat("import_adjudicat");
				PreparedStatement st = conexion_dw.prepareStatement("insert into ObjetoContrato (objetoContrato, importe) values (?,?)");
				st.setString(1, objeto_contrato);
				st.setFloat(2, importe);
				st.execute();
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private void crearTablaTipoContrato(){
		ResultSet result = null;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select tipus_contracte from CONTRATOS GROUP BY tipus_contracte");
			result = stt.executeQuery();
			while (result.next()) {
				String tipo_contrato=result.getString("tipus_contracte");

				PreparedStatement st = conexion_dw.prepareStatement("insert into TipoContrato (tipoContrato) values (?)");
				st.setString(1, tipo_contrato);
				st.execute();


			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private void crearTablaOrganoContratante(){
		ResultSet result = null;
		try {
			PreparedStatement stt = conexion_aux.prepareStatement("select organ_contractant from CONTRATOS GROUP BY organ_contractant");
			result = stt.executeQuery();
			while (result.next()) {
				String organo=result.getString("organ_contractant");

				PreparedStatement st = conexion_dw.prepareStatement("insert into OrganoContratante (organoContratante) values (?)");
				st.setString(1, organo);
				st.execute();
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private void crearHechosBD(){
		ResultSet result = null;
		ResultSet result2 = null;

		try {
			PreparedStatement st2;
			PreparedStatement st = conexion_aux.prepareStatement("select * from CONTRATOS");
			result = st.executeQuery();
			while (result.next()) {
				String TipoEns=result.getString("tipus_ens"); 
				String OrganoContratante=result.getString("organ_contractant"); 
				String TipoContrato=result.getString("tipus_contracte"); ;
				String FechaAdjudicacion=result.getString("data_adjudicacio");  
				String Proveedor=result.getString("proveidor");  
				String NIF=result.getString("nif"); ; 
				String ObjetoDeContrato=result.getString("objecte_del_contracte");        		 



				//fecha
				int FechaH=0;
				if(!FechaAdjudicacion.equals("")){
					String [] f=FechaAdjudicacion.split("/");
					int dia=Integer.parseInt(f[0]);
					int mes=Integer.parseInt(f[1]);
					int anio=Integer.parseInt(f[2]);
					String dia_str = "";
					dia_str = (dia<10) ? "0" + Integer.toString(dia) : Integer.toString(dia);
				
					String mes_str = "";
					mes_str = (mes<10) ? "0" + Integer.toString(mes) : Integer.toString(mes);
				
					String id="";
					id=anio+""+mes_str+""+dia_str+"";
					FechaH=Integer.parseInt(id);
				}
				else{
					FechaH=0;
				}

				//proveedor
				int ProveedorH=0;
				st2 = conexion_dw.prepareStatement("select id_proveedor from Proveedor where NIF = ? and nombre_prov = ?");
				st2.setString(1, NIF);
				st2.setString(2, Proveedor);

				result2 = st2.executeQuery();
				while (result2.next()) {
					ProveedorH=result2.getInt("id_proveedor");
				}

				//tipo_ens
				int TipoEnsH=0;
				st2 = conexion_dw.prepareStatement("select id_tipoEns from TipoEns where tipoEns=?");
				st2.setString(1, TipoEns);
				result2 = st2.executeQuery();
				TipoEnsH=result2.getInt("id_tipoEns");

				//objeto contrato
				int ObjetoContratoH=0;
				st2 = conexion_dw.prepareStatement("select id_objetoContrato from ObjetoContrato where objetoContrato=?");
				st2.setString(1, ObjetoDeContrato);
				result2 = st2.executeQuery();
				ObjetoContratoH=result2.getInt("id_objetoContrato");

				//tipo contrato
				int TipoContratoH=0;
				st2 = conexion_dw.prepareStatement("select id_tipoContrato from TipoContrato where tipoContrato=?");
				st2.setString(1, TipoContrato);
				result2 = st2.executeQuery();
				TipoContratoH=result2.getInt("id_tipoContrato");

				//organo contratante
				int OrganoContratanteH=0;
				st2 = conexion_dw.prepareStatement("select id_organoContratante from OrganoContratante where organoContratante=?");
				st2.setString(1, OrganoContratante);
				result2 = st2.executeQuery();
				OrganoContratanteH=result2.getInt("id_organoContratante");


				PreparedStatement st3 = conexion_dw.prepareStatement("insert into Contrato (id_proveedor, id_fecha, id_tipoEns, id_objetoContrato,id_tipoContrato, id_organoContratante) values (?,?,?,?,?,?)");
				st3.setInt(1, ProveedorH);
				st3.setInt(2, FechaH);
				st3.setInt(3, TipoEnsH);
				st3.setInt(4, ObjetoContratoH);
				st3.setInt(5, TipoContratoH);
				st3.setInt(6, OrganoContratanteH);
				st3.execute();

			}
		} catch (SQLException ex) {
			System.err.println("Error en crearHechos");
			System.err.println(ex.getMessage());
		}
	}



	private void CalcularMedidas(){

		ResultSet result = null;
		ResultSet result2 = null;
		ResultSet result4 = null;
		
		float [] medidas= new float[5];
		
		try {
			int idContrato=0;                     
			int idFecha=0;
			int mes=0;
			PreparedStatement st;
			PreparedStatement st2 = conexion_dw.prepareStatement("select * from Contrato");
			result4 = st2.executeQuery();
			while (result4.next()) {
				idContrato=result4.getInt("id_contrato");                     
				idFecha=result4.getInt("id_fecha"); 

				st2 = conexion_dw.prepareStatement("select mes from Fecha where id_fecha=?");
				st2.setInt(1, idFecha);
				result2 = st2.executeQuery();
				mes=result2.getInt("mes");

				//consultas para cada medida
				String rr="";
				String consulta="";

				//1. Total de contratos en un mes     
				consulta="Select count(id_proveedor) from Contrato where id_fecha IN (select id_fecha from Fecha where  mes="+mes+")"; 
				st = conexion_dw.prepareStatement(consulta);

				result = st.executeQuery();
				while (result.next()) {
					rr=result.getString("count(id_proveedor)"); 
				}
				medidas[0]=Float.parseFloat(rr);

				//2. Total importe por mes     
				consulta="Select sum (importe) AS suma from ObjetoContrato where id_objetoContrato IN(select id_objetoContrato from Contrato where id_fecha IN (select id_fecha from Fecha where mes="+mes+"))"; 
				st = conexion_dw.prepareStatement(consulta);

				result = st.executeQuery();
				while (result.next()) {
					rr=result.getString("suma"); 
				}
				medidas[1]=Float.parseFloat(rr);

				//3. Media del importe de los contratos en un mes     
				consulta="Select AVG (importe) AS media from ObjetoContrato where id_objetoContrato IN(select id_objetoContrato from Contrato where id_fecha IN (select id_fecha from Fecha where mes="+mes+"))"; 
				st = conexion_dw.prepareStatement(consulta);

				result = st.executeQuery();
				while (result.next()) {
					rr=result.getString("media"); 
				}
				medidas[2]=Float.parseFloat(rr);

				//4. Contrato mas caro de un mes     
				consulta="Select MAX (importe) AS maximo from ObjetoContrato where id_objetoContrato IN(select id_objetoContrato from Contrato where id_fecha IN (select id_fecha from Fecha where mes="+mes+"))"; 
				st = conexion_dw.prepareStatement(consulta);

				result = st.executeQuery();
				while (result.next()) {
					rr=result.getString("maximo"); 
				}
				medidas[3]=Float.parseFloat(rr);

				//5. Contrato mas barato de un mes     
				consulta="Select MIN (importe) AS minimo from ObjetoContrato where id_objetoContrato IN(select id_objetoContrato from Contrato where id_fecha IN (select id_fecha from Fecha where mes="+mes+"))"; 
				st = conexion_dw.prepareStatement(consulta);

				result = st.executeQuery();
				while (result.next()) {
					rr=result.getString("minimo"); 
				}
				medidas[4]=Float.parseFloat(rr);

				//metemos las medidas
				PreparedStatement st3 = conexion_dw.prepareStatement("update contrato set totalContratos=?, totalImporte=?, mediaImporte=?, importeMax=?, importeMin=? where id_contrato=?");
				st3.setInt(1, Math.round(medidas[0]));
				st3.setFloat(2, medidas[1]);
				st3.setFloat(3, medidas[2]);
				st3.setFloat(4, medidas[3]);
				st3.setFloat(5, medidas[4]);
				st3.setInt(6, idContrato);
				st3.executeUpdate();
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}
	
	public static Date ParseFecha(String fecha)
	{
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaDate = null;
		if(fecha != "" || fecha != null){
			try {
				fechaDate = formato.parse(fecha);
			} 
			catch (ParseException ex) 
			{
				ex.printStackTrace();
			}
		}
		return fechaDate;
	}
}
