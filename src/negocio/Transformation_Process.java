/**
 * 
 */
package negocio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import persistencia.ConexionSQLite;

/**
 * @author Sergio Fernández
 *
 */
public class Transformation_Process {

	private List<Contrato> lista_contratos = new ArrayList<Contrato>();
	private ConexionSQLite cn;
	private Connection conexion;
	private String data;
	
	// Soluciona la omisión de los datos de los proveedores y el nif de los mismos	
	private static final String AGRUPAR_PROVEEDORES_POR_NIF = "SELECT nif, count(nif) AS suma FROM CONTRATOS WHERE nif  NOT LIKE '' OR NOT nif = null GROUP BY nif ORDER BY suma DESC";
	private static final String PROVEEDORES_CON_NIF_X = "SELECT DISTINCT proveidor, nif  FROM CONTRATOS WHERE nif  = '";
	
	private static final String TIPO_CONTRACTO_DESCONOCIDO = "UPDATE CONTRATOS SET tipus_contracte= 'DESCONICIDO' WHERE tipus_contracte = ''";
	private static final String TIPO_CONTRACTO_SERVEIS = "UPDATE CONTRATOS SET tipus_contracte= 'SERVEIS' WHERE  tipus_contracte like '%ser%'";
	private static final String TIPO_CONTRACTO_OBRES = "UPDATE CONTRATOS SET tipus_contracte= 'OBRES' WHERE  tipus_contracte like '%obr%'";
	private static final String TIPO_CONTRACTO_ALTRES = "UPDATE CONTRATOS SET tipus_contracte= 'ALTRES' WHERE  tipus_contracte like 'alt%'";
	private static final String TIPO_CONTRACTO_PRIVATS = "UPDATE CONTRATOS SET tipus_contracte= 'PRIVATS' WHERE  tipus_contracte like '%privat%'";
	private static final String TIPO_CONTRACTO_SUBMINISTRAMENTS = "UPDATE CONTRATOS SET tipus_contracte= 'SUBMINISTRAMENTS' WHERE  tipus_contracte like '%sub%' or tipus_contracte like '%sum%'  or tipus_contracte like 's'";
	
	//Revisar y tomar decisiones finales sobre los cambios
	private static final String PROVEDOR_FANTASMA = "UPDATE CONTRATOS SET proveidor = 'DESCONOCIDO' WHERE proveidor = '' AND nif =''";
	private static final String NIF_INCORRECTO = "UPDATE CONTRATOS SET nif = '00000000-?' WHERE nif LIKE ''";
	
	//private static final String  = "UPDATE CONTRATOS SET proveidor = 'DESCONOCIDO' WHERE proveidor = ''";
	private static final String [] TRANFORMACIONES = {PROVEDOR_FANTASMA, NIF_INCORRECTO, TIPO_CONTRACTO_DESCONOCIDO, TIPO_CONTRACTO_SERVEIS, TIPO_CONTRACTO_OBRES,
			TIPO_CONTRACTO_ALTRES, TIPO_CONTRACTO_PRIVATS, TIPO_CONTRACTO_SUBMINISTRAMENTS}; 

	
	public Transformation_Process(List<Contrato> contratos, String path_data_aux ) throws ClassNotFoundException, SQLException{
		this.lista_contratos = contratos;
		data = path_data_aux;
		//Conectamos con la base de datos auxiliar para hacer las transformaciones.
		cn = new ConexionSQLite(path_data_aux);
		conexion = cn.getConexion();
	}
	
	protected void execute(List<Contrato> l_contratos) throws SQLException, ClassNotFoundException{
		this.lista_contratos = l_contratos;
		copiarATabla();
		adecuacionEnTabla();
	}
	
	 private void adecuacionEnTabla() throws SQLException, ClassNotFoundException {
		 int i = 0;
		 PreparedStatement st;
		 ResultSet result = null;

		 //Agrupamos por nif y nos quemamos con el nombre de proveedor más largo
		 //Para después dejarlo igual en todas las coincidencias, así se solucionan problema¡
		 // de inconsistencia.
		 cn = new ConexionSQLite(data);
		 conexion = cn.getConexion();
		 st= conexion.prepareStatement(AGRUPAR_PROVEEDORES_POR_NIF);
		 result = st.executeQuery();
		 
		 
		 seleccionarProveerdor(result);
		 
		 
		 for(i=0; i<TRANFORMACIONES.length;i++){
			 st = conexion.prepareStatement(TRANFORMACIONES[i]);
			 st.execute();
			 
		 }
		 conexion.close();

		 
		 
	}

	private void seleccionarProveerdor(ResultSet result) throws SQLException {
		String nif;
		String seleccion= "";
		PreparedStatement st;
		ResultSet result_proveedores;

		while(result.next()){
			
			conexion = cn.getConexion();
			st = conexion.prepareStatement(PROVEEDORES_CON_NIF_X + result.getString("nif") + "'");
			result_proveedores = st.executeQuery();
			
			nif = result.getString("nif");
			seleccion = mostrarElegirProveedor(result_proveedores, nif);
			
			if(seleccion.contains("'"))seleccion = seleccion.replaceAll("'", "''");
			if(seleccion.contains("\""))seleccion = seleccion.replaceAll("\"", "\"\"");
			
			System.out.println("\tSeleccion: " + seleccion);
			st = conexion.prepareStatement("UPDATE CONTRATOS SET proveidor ='"+ seleccion +"' WHERE nif='" + nif + "'");
			st.execute();
		}
		conexion.close();
		
	}

	private String mostrarElegirProveedor(ResultSet result, String nif) throws SQLException {
		String [] lista_sociedades = {",SL", ",S.L", ",SL.", ",S.L.",
										", SL", ", S.L", ", SL.", ", S.L.",
										"SL", "S.L", "SL.", "S.L." , 
										",SA", ",S.A", ",SA.", ",S.A.",
										", SA",", S.A", ", SA.", ", S.A.",
										"SA", "S.A", "SA.", "S.A."};
		
		int i= 0;
		int max_cadena = 0;
		int n_opciones = 0;
		int opcion = -1;
		String eleccion = "";
		
		LinkedList<String> lista_proveedores = new LinkedList<String>();
		
		while (result.next()){
			lista_proveedores.add(result.getString("proveidor"));
		}
		n_opciones = lista_proveedores.size();
		
		if(n_opciones > 1){
			for(i=0; i<n_opciones;i++){
				//Seleccionamos los nombres de los proveedores dependiendo de la mejor adecuación del nombre
				//Los criterios que se han tomado son los siguientes:
				//	1.- Comprobamos si tienen las cadenas :
				//			{SL, S.L, SL., S.L. , ", SL", ", S.L", ", SL.", ", S.L.", ",SL", ",S.L", ",SL.", ",S.L.",
				// 				SA, S.A, SA., S.A. , ", SA", ", S.A", ", SA.", ", S.A.", ",SA", ",S.A", ",SA.", ",S.A."}
				//	2.- Si tienen alguna de éstas:
				//		2.1 se adeacua al sigiente formato: " S.[A,L]."
				// 	3.- Se toma la cadena más descriptiva, es decir la que tiene mayor longitud.
				
				//(1, 2)
				lista_proveedores.set(i, lista_proveedores.get(i).trim());
				lista_proveedores.set(i, contieneAlguna(lista_proveedores.get(i),lista_sociedades));
				
				//Se adecuan las comas y después se elimina cualquier espacio sobrante que peda haber. 
				lista_proveedores.set(i, lista_proveedores.get(i).replaceAll(" ,", ", "));
				lista_proveedores.set(i, lista_proveedores.get(i).replaceAll(" +", " "));
				
				//(3)
				if(lista_proveedores.get(i).length() > max_cadena){
					max_cadena = lista_proveedores.get(i).length();
					opcion=i;
				}
			}

			if(opcion>=0 && opcion<n_opciones){
				eleccion = lista_proveedores.get(opcion).trim();
			}
		}
		else{
			//(1, 2)
			lista_proveedores.set(0, contieneAlguna(lista_proveedores.get(i),lista_sociedades));
			//Se adecuan las comas y después se elimina cualquier espacio sobrante que peda haber. 
			lista_proveedores.set(0, lista_proveedores.get(i).replaceAll(" ,", ", "));
			lista_proveedores.set(0, lista_proveedores.get(i).replaceAll(" +", " "));
			eleccion = lista_proveedores.get(0);
		}
		
		return eleccion;
	}

	private String contieneAlguna(String cadena, String[] lista_cadenas) {
		int i = 0;
		int tamanio = lista_cadenas.length;
		int tail_size = 0;
		int cadena_size = cadena.length();
		
		while(i<tamanio){
			if(cadena.contains(lista_cadenas[i])){		
				tail_size = lista_cadenas[i].length();
				cadena = cadena.substring(0, cadena_size-tail_size);
				
				if(lista_cadenas[i].toUpperCase().contains("A")){
					cadena = cadena.concat(" S.A.");
					return cadena;
				}else{
					cadena = cadena.concat(" S.L.");
					return cadena;
				}
			}
			i++;
		}
		return cadena;
	}

	private void copiarATabla() throws SQLException {
		 int i = 1;
		 int tamanio = lista_contratos.size();
		 //int tamanio = 201;
		 float porcentaje = 0;

		//Borramos los datos anteriores.
		 System.out.println("Eliminando valores anteriores");
		 PreparedStatement  st = conexion.prepareStatement("DELETE FROM CONTRATOS");
		 st.execute();
		 System.out.println("Datos eliminados");
		 //Reiniciamos la clave principal.
		 st = conexion.prepareStatement("UPDATE sqlite_sequence SET seq=0");
		 st.execute();

		 System.out.println("Insertando los " + (tamanio-1) +" datos.");
		 for(i=1; i<tamanio;i++){	
			 if(i%10==0 || i == tamanio-1){
				 porcentaje = (float)((float)i/(float)tamanio)*100;
				 System.out.printf("Completado %3.2f%% %n", porcentaje);
			 }
			 st = conexion.prepareStatement(
					 "insert into CONTRATOS ("+
							 "tipus_ens, organ_contractant, tipus_contracte,"+
							 "data_adjudicacio, proveidor, nif, objecte_del_contracte,"+
							 "import_adjudicat) values (" + 
							 lista_contratos.get(i).toString()+", "+ lista_contratos.get(i).getImporte_adjudicado()+")");
			 st.execute();
		 }
		 System.out.println("Completado 100%");
		 conexion.close();
	 }
}
