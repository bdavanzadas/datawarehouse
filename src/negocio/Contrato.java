package negocio;

public class Contrato {
	private String tipo_ens; 
	private String organo_contratante; 
	private String tipo_contrato; 
	private String fecha_adjudicacion; 
	private String proveedor; 
	private String nif; 
	private String objeto_de_contrato; 
	private float importe_adjudicado;
	
	public Contrato(String tipoEns, String organoContratante, String tipoContrato, String fechaAdjudicacion,
			String proveedor,String nif, String objetoDeContrato, float importeAdjudicado) {
		this.tipo_ens = tipoEns;
		this.organo_contratante = organoContratante;
		this.tipo_contrato = tipoContrato;
		this.fecha_adjudicacion = fechaAdjudicacion;
		this.proveedor = proveedor;
		this.nif = nif;
		this.objeto_de_contrato = objetoDeContrato;
		this.importe_adjudicado = importeAdjudicado;
	}

	//Getters and setters

	/**
	 * @return the tipo_ens
	 */
	public String getTipo_ens() {
		return tipo_ens;
	}

	/**
	 * @param tipo_ens the tipo_ens to set
	 */
	public void setTipo_ens(String tipo_ens) {
		this.tipo_ens = tipo_ens;
	}

	/**
	 * @return the organo_contratante
	 */
	public String getOrgano_contratante() {
		return organo_contratante;
	}

	/**
	 * @param organo_contratante the organo_contratante to set
	 */
	public void setOrgano_contratante(String organo_contratante) {
		this.organo_contratante = organo_contratante;
	}

	/**
	 * @return the tipo_contrato
	 */
	public String getTipo_contrato() {
		return tipo_contrato;
	}

	/**
	 * @param tipo_contrato the tipo_contrato to set
	 */
	public void setTipo_contrato(String tipo_contrato) {
		this.tipo_contrato = tipo_contrato;
	}

	/**
	 * @return the fecha_adjudicacion
	 */
	public String getFecha_adjudicacion() {
		return fecha_adjudicacion;
	}

	/**
	 * @param fecha_adjudicacion the fecha_adjudicacion to set
	 */
	public void setFecha_adjudicacion(String fecha_adjudicacion) {
		this.fecha_adjudicacion = fecha_adjudicacion;
	}

	/**
	 * @return the proveedor
	 */
	public String getProveedor() {
		return proveedor;
	}

	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	/**
	 * @return the nif
	 */
	public String getNif() {
		return nif;
	}

	/**
	 * @param nif the nif to set
	 */
	public void setNif(String nif) {
		this.nif = nif;
	}

	/**
	 * @return the objeto_de_contrato
	 */
	public String getObjeto_de_contrato() {
		return objeto_de_contrato;
	}

	/**
	 * @param objeto_de_contrato the objeto_de_contrato to set
	 */
	public void setObjeto_de_contrato(String objeto_de_contrato) {
		this.objeto_de_contrato = objeto_de_contrato;
	}

	/**
	 * @return the importe_adjudicado
	 */
	public float getImporte_adjudicado() {
		return importe_adjudicado;
	}

	/**
	 * @param importe_adjudicado the importe_adjudicado to set
	 */
	public void setImporte_adjudicado(float importe_adjudicado) {
		this.importe_adjudicado = importe_adjudicado;
	}

	@Override
	public String toString() {
		return " \""+ tipo_ens + "\" , \"" + organo_contratante + "\" , \""
				+ tipo_contrato + "\" , \"" + fecha_adjudicacion + "\" , \""
				+ proveedor + "\" , \"" + nif + "\" , \""
				+ objeto_de_contrato + "\"";
	}
	
}