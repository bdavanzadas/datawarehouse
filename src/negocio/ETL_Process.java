/**
 * 
 */
package negocio;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Sergio Fernández.
 *
 */

public class ETL_Process {
	
	private Extraction_Process extrac_process;
	private Transformation_Process trans_process;
	private Load_Process load_porcess;
	private List<Contrato> lista_contratos;
	
	public ETL_Process(String [] sources,String path_data_aux, String path_data_warehouse) throws ClassNotFoundException, SQLException{
		extrac_process = new Extraction_Process(sources);
		trans_process = new Transformation_Process(null, path_data_aux);
		load_porcess = new Load_Process(path_data_aux, path_data_warehouse);
	}

	public void executeETL() throws ClassNotFoundException, SQLException{
		executeExtraction();
		executeTransformationAndLoad();
	}
	
	public void  executeExtraction() {
		lista_contratos = extrac_process.execute();
	}

	public void executeTransformationAndLoad() throws ClassNotFoundException, SQLException {
		trans_process.execute(lista_contratos);
		load_porcess.execute();
	}
}