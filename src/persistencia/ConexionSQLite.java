package persistencia;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;   

public class ConexionSQLite {
	private Connection conexion;
	
	public ConexionSQLite(String source) throws ClassNotFoundException, SQLException{  	
		Class.forName("org.sqlite.JDBC");
		conexion = DriverManager.getConnection("jdbc:sqlite:" + source);
	}
	/**
	 * @return the conexion
	 */
	public Connection getConexion() {
		return conexion;
	}
}