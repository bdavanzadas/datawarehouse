>Ejercicio práctico de un almacén de datos (*Data Warehouse*) para la asignatura de "Bases de Datos Avanzadas".
>
>*ESI - Ciudad Real, UCLM.*

>##Autores:
>+ [Sergio Fernández García.](https://bitbucket.org/Sergio_Fdz/)
>+ [Pilar García Martín de la Puente.](https://bitbucket.org/pilar_gm/)
>
>##Fuentes:
>[http://opendata-ajuntament.barcelona.cat/data/es/dataset/contractes-menors](http://opendata-ajuntament.barcelona.cat/data/es/dataset/contractes-menors)
>